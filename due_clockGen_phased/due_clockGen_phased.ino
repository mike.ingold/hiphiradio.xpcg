#include <Wire.h>
#include "src/SparkFun_5P49V60.h"

// Constant parameters
const float    VCO_Freq    = 1600.0;   // MHz
const float    Ch2_Freq    = 16.0;     // MHz
const float    Ch3_Freq    = 16.0;     // MHz
const uint8_t  Pin_SDOE    = 40;       // Physical pin number for shutdown/output-enable (SD/OE)
const uint32_t I2C_Rate    = 115200;   // baud
const uint32_t Alive_Delay = 10000;    // 10,000 ms = 10 s

SparkFun_5P49V60 clockGen;
bool config_status_SDOE = 0;
float config_ch3_skew_deg = 45.0;

// Set status (ENABLE or DISABLE) of SD/OE pin.
void set_SDOE(uint8_t status)
{
    if ( status == ENABLE ) {
        // Intent to enable -> set pin logic level HIGH
        digitalWrite(Pin_SDOE, LOW);
        config_status_SDOE = true;
    } else if ( status == DISABLE ) {
        // Intent to disable or erroneoud input -> set pin logic level LOW
        digitalWrite(Pin_SDOE, HIGH);
        config_status_SDOE = false;
    }
}

// Run once at power-on
void setup()
{
    // Ready the OE (clk output enable) pin
    pinMode(Pin_SDOE, OUTPUT);
    set_SDOE(DISABLE);

    // Initiate debugging TTY over USB
    Wire.begin();
    Serial.begin(I2C_Rate);
    Serial.println("Hello!");

    // Initiate I2C communication with Clock Generator board
    if (clockGen.begin() == true) {
        // Connection established
        Serial.print("Established I2C connection to Clock Generator (0x");
        Serial.print(clockGen.readI2CAddress(), HEX);
        Serial.print(").\n");
    } else {
        // Connection failed -> HALT
        Serial.println("ERROR. I2C connection to Clock Generator failed. Halting.");
        while(1);
    }

    // Configure Clock Generator VCO
    Serial.println("Configuring Internal Clock Frequency to 1600MHz.");
    clockGen.setVcoFrequency(VCO_Freq);

    // Configure Channel 2
    Serial.println("Configuring Channel 2:");
    Serial.print("    Frequency = "); Serial.print(Ch2_Freq); Serial.print(" MHz\n");
    clockGen.muxPlltoFod(CHANNEL_2, ENABLE);
    clockGen.clockConfigMode(CHANNEL_2, CMOS_MODE);
    clockGen.setClockFreq(CHANNEL_2, Ch2_Freq);
    clockGen.setClockVoltage(CHANNEL_2, THREE_THREE_V);

    // Configure Channel 3
    Serial.println("Configuring Channel 3:");
    Serial.print("    Frequency = "); Serial.print(Ch3_Freq); Serial.print(" MHz\n");
    Serial.print("    Phase Shift = "); Serial.print(config_ch3_skew_deg); Serial.print(" degrees\n");
    clockGen.muxPlltoFod(CHANNEL_3, ENABLE);
    clockGen.clockConfigMode(CHANNEL_3, CMOS_MODE);
    clockGen.setClockFreq(CHANNEL_3, Ch3_Freq);
    clockGen.setClockVoltage(CHANNEL_3, THREE_THREE_V);
    clockGen.skewClock(CHANNEL_3, config_ch3_skew_deg);

    // TEMPORARY FOR DEBUGGING
    Serial.print("Skew Variables (Int,Frac) = (");
    Serial.print(clockGen.readIntDivSkew(CHANNEL_3));
    Serial.print(", ");
    Serial.print(clockGen.readFractDivSkew(CHANNEL_3));
    Serial.print(")\n");

    // Enable outputs
    Serial.println("Enabling SD/OE pin");
    set_SDOE(ENABLE);
}

// Run forever in a loop after setup()
void loop()
{
    Serial.println("Still alive...");
    delay(Alive_Delay);
}

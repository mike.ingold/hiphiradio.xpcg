/*
Notice: this code is forked from a library developed by SparkFun and carries the following license:

The MIT License (MIT)

Copyright (c) 2016 SparkFun Electronics, Elias Santistevan

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef _SPARKFUN_CLOCK_5P49V60_
#define _SPARKFUN_CLOCK_5P49V60_

#include <Wire.h>
#include <Arduino.h>
#include <math.h>

// PARAMETERS SPECIFIC TO SPARKFUN BREAKOUT BOARD
//                            1 , 2   , 4   , 8    , 16   , 32
static float _cap_arr[6] = {.43 , .43 , .86 , 1.73 , 3.46 , 6.92};
static uint8_t DEF_CLOCK = 16; // 16 MHz

/**************************************************************************************************
 *  CLOCK GENERATOR REGISTER SETTINGS
 *      VersaClock 6E Family Register Descriptions and Programming Guide, August 30, 2018
 *************************************************************************************************/

// Reference Clock Source (Table 25)
enum CLOCKGEN_SOURCE
{
	CLOCK_SOURCE = 0x00,    // External reference clock
	XTAL_SOURCE  = 0x01     // Crystal oscillator
};

// Clock Generator IC's I2C Address
enum CLOCKGEN_I2C_ADDRESS
{
	I2C_DEFAULT   = 0x6A,    // 0xD4 in 7bit
	I2C_ALTERNATE = 0x68     // 0xD0 in 7bit
};

// CLOCK OUTPUT CHANNEL
enum CLOCK_CHANNEL
{
	CHANNEL_0 = 0x00,
	CHANNEL_1 = 0x01,
	CHANNEL_2 = 0x02,
	CHANNEL_3 = 0x03,
	CHANNEL_4 = 0x04
};

// CLOCK SIGNAL TYPE (Table 108)
enum CLOCK_MODE
{
	LVPECL_MODE  = 0x00,
	CMOS_MODE    = 0x01,    // Single-ended CMOS on primary pin
	HCSL33_MODE  = 0x02,
	LVDS_MODE    = 0x03,
	CMOS2_MODE   = 0x04,    // Single-ended CMOS on secondary/B pin
	CMOSD_MODE   = 0x05,    // Differential CMOS
	HCSL25_MODE  = 0x06
};

// CMOS SLEW RATE (Table 108)
enum CLOCK_SLEWRATE
{
	SLOWEST  = 0x00,    // 0.80x
	SLOW     = 0x01,    // 0.85x
	FAST     = 0x02,    // 0.90x
	FASTEST  = 0x03     // 1.00x
};

// CMOS SIGNAL DRIVE VOLTAGE (Table 108)
enum CLOCK_VOLTAGE
{
	ONE_EIGHT_V    = 0x00,    // 1.8V
	TWO_FIVE_V     = 0x02,    // 2.5V
	THREE_THREE_V  = 0x03     // 3.3V
};

// OTHERS
#define  ALT              0x01
#define  DEF              0x00
#define  DISABLE          0x00
#define  ENABLE           0x01
#define  XIN              0x01
#define  XOUT             0x02
#define  ERROR            0xFF
#define  ERROR_16B        0xFFFF
#define  ERROR_32B        0xFFFFFF
#define  UNKNOWN_ERROR    0xFF
#define  UNKNOWN_ERROR_F  255.0

/**************************************************************************************************
 *  CLOCK GENERATOR REGISTER ADDRESS MAP
 *      VersaClock 6E Family Register Descriptions and Programming Guide, August 30, 2018
 *************************************************************************************************/
enum REGISTER_INDEX
{
	// MAIN CONFIGURATION
	OTP_CONTROL_REG       = 0x00,
	SHUTDOWN_REG          = 0x10,    // Primary Source and Shutdown (Table 25)
	VCO_BAND_REG          = 0x11,    // VCO Band (Table 33)
	LOAD_CAP_REG1         = 0x12,    // Crystal X1 Load Capacitor (Table 28)
	LOAD_CAP_REG2         = 0x13,    // Crystal X2 Load Capacitor (Table 29)
	REF_DIVIDER_REG       = 0x15,    // Reference Divider (Table 31)
	DIVIDER_VCO_REG       = 0x16,    // VCO Control and Pre-Divider (Table 32)
	FDB_INT_DIV_REG1      = 0x17,    // Feedback Integer Divider (Table 34)
	FDB_INT_DIV_REG2      = 0x18,    // Feedback Integer Divider Bits (Table 35)
	FDB_FRAC_DIV_REG1     = 0x19,    // Feedback Fractional Divider (Table 36)
	FDB_FRAC_DIV_REG2     = 0x1A,    // Feedback Fractional Divider Bits (Table 37)
	FDB_FRAC_DIV_REG3     = 0x1B,    // Feedback Fractional Divider (Table 38)
	VC_CONTROL_REG        = 0x1C,    // VCO Control (Table 39)
	RC_CONTR_REG1         = 0x1D,    // VCO & Charge Pump (Table 40)
	RC_CONTR_REG2         = 0x1E,    // RC Control (PLL Loop Filter) (Table 41)
	RC_CONTR_REG3         = 0x1F,    // RC Control (Table 42)
	CLK_OE_FUNC_REG       = 0x68,    // CLK_OE/Shutdown Function (Table 26)
	CLK_OS_FUNC_REG       = 0x69,    // CLK_OS/Shutdown Function (Table 27)
	GLOBAL_RESET_REG      = 0x76,     // Global reset bit (pg. 49)

	// OUTPUT DIVIDER 1
	DIV1_CONTROL_REG      = 0x21,    // Control (Table 47)
	OUT1_FDIV_REG1        = 0x22,    // Fractional Settings (Table 59)
	OUT1_FDIV_REG2        = 0x23,    // Fractional Settings (Table 60)
	OUT1_FDIV_REG3        = 0x24,    // Fractional Settings (Table 61)
	OUT1_FDIV_REG4        = 0x25,    // Fractional Settings (Table 62)
	OUT1_STEP_SPRD_REG1   = 0x26,    // Step Spread Spectrum Config (Table 63)
	OUT1_STEP_SPRD_REG2   = 0x27,    // Step Spread Spectrum Config (Table 64)
	OUT1_STEP_SPRD_REG3   = 0x28,    // Step Spread Spectrum Config (Table 65)
	OUT1_SPRD_RATE_REG1   = 0x29,    // Spread Modulation Rate Config (Table 66)
	OUT1_SPRD_RATE_REG2   = 0x2A,    // Spread Modulation Rate Config (Table 67)
	OUT1_ISKEW_REG1       = 0x2B,    // Skew Integer Part (Table 95)
	OUT1_ISKEW_REG2       = 0x2C,    // Skew Integer Part (Table 96)
	OUT1_IDIV_REG1        = 0x2D,    // Integer Part (Table 51)
	OUT1_IDIV_REG2        = 0x2E,    // Integer Part (Table 52)
	OUT1_FSKEW_REG        = 0x2F,    // Skew Fractional Part (Table 97)

	// OUTPUT DIVIDER 2
	DIV2_CONTROL_REG      = 0x31,    // Control (Table 48)
	OUT2_FDIV_REG1        = 0x32,    // Fractional Settings (Table 68)
	OUT2_FDIV_REG2        = 0x33,    // Fractional Settings (Table 69)
	OUT2_FDIV_REG3        = 0x34,    // Fractional Settings (Table 70)
	OUT2_FDIV_REG4        = 0x35,    // Fractional Settings (Table 71)
	OUT2_STEP_SPRD_REG1   = 0x36,    // Step Spread Spectrum Config (Table 72)
	OUT2_STEP_SPRD_REG2   = 0x37,    // Step Spread Spectrum Config (Table 73)
	OUT2_STEP_SPRD_REG3   = 0x38,    // Step Spread Spectrum Config (Table 74)
	OUT2_SPRD_RATE_REG1   = 0x39,    // Spread Modulation Rate Config (Table 75)
	OUT2_SPRD_RATE_REG2   = 0x3A,    // Spread Modulation Rate Config (Table 76)
	OUT2_ISKEW_REG1       = 0x3B,    // Skew Integer Part (Table 98)
	OUT2_ISKEW_REG2       = 0x3C,    // Skew Integer Part (Table 99)
	OUT2_IDIV_REG1        = 0x3D,    // Integer Part (Table 53)
	OUT2_IDIV_REG2        = 0x3E,    // Integer Part (Table 54)
	OUT2_FSKEW_REG        = 0x3F,    // Skew Fractional Part (Table 100)

	// OUTPUT DIVIDER 3
	DIV3_CONTROL_REG      = 0x41,    // Control (Table 49)
	OUT3_FDIV_REG1        = 0x42,    // Fractional Settings (Table 77)
	OUT3_FDIV_REG2        = 0x43,    // Fractional Settings (Table 78)
	OUT3_FDIV_REG3        = 0x44,    // Fractional Settings (Table 79)
	OUT3_FDIV_REG4        = 0x45,    // Fractional Settings (Table 80)
	OUT3_STEP_SPRD_REG1   = 0x46,    // Step Spread Spectrum Config (Table 81)
	OUT3_STEP_SPRD_REG2   = 0x47,    // Step Spread Spectrum Config (Table 82)
	OUT3_STEP_SPRD_REG3   = 0x48,    // Step Spread Spectrum Config (Table 83)
	OUT3_SPRD_RATE_REG1   = 0x49,    // Spread Modulation Rate Config (Table 84)
	OUT3_SPRD_RATE_REG2   = 0x4A,    // Spread Modulation Rate Config (Table 85)
	OUT3_ISKEW_REG1       = 0x4B,    // Skew Integer Part (Table 101)
	OUT3_ISKEW_REG2       = 0x4C,    // Skew Integer Part (Table 102)
	OUT3_IDIV_REG1        = 0x4D,    // Integer Part (Table 55)
	OUT3_IDIV_REG2        = 0x4E,    // Integer Part (Table 56)
	OUT3_FSKEW_REG        = 0x4F,    // Skew Fractional Part (Table 103)

	// OUTPUT DIVIDER 4
	DIV4_CONTROL_REG      = 0x51,    // Control (Table 50)
	OUT4_FDIV_REG1        = 0x52,    // Fractional Settings (Table 86)
	OUT4_FDIV_REG2        = 0x53,    // Fractional Settings (Table 87)
	OUT4_FDIV_REG3        = 0x54,    // Fractional Settings (Table 88)
	OUT4_FDIV_REG4        = 0x55,    // Fractional Settings (Table 89)
	OUT4_STEP_SPRD_REG1   = 0x56,    // Step Spread Spectrum Config (Table 90)
	OUT4_STEP_SPRD_REG2   = 0x57,    // Step Spread Spectrum Config (Table 91)
	OUT4_STEP_SPRD_REG3   = 0x58,    // Step Spread Spectrum Config (Table 92)
	OUT4_SPRD_RATE_REG1   = 0x59,    // Spread Modulation Rate Config (Table 93)
	OUT4_SPRD_RATE_REG2   = 0x5A,    // Spread Modulation Rate Config (Table 94)
	OUT4_ISKEW_REG1       = 0x5B,    // Skew Integer Part (Table 105)
	OUT4_ISKEW_REG2       = 0x5C,    // Skew Integer Part (Table 106)
	OUT4_IDIV_REG1        = 0x5D,    // Integer Part (Table 57)
	OUT4_IDIV_REG2        = 0x5E,    // Integer Part (Table 58)
	OUT4_FSKEW_REG        = 0x5F,    // Skew Fractional Part (Table 107)

	// CLOCK OUTPUT CONFIGURATION
	CLK1_OUT_CNFIG_REG1   = 0x60,    // Clock1 Output Configuration (Table 108)
	CLK1_OUT_CNFIG_REG2   = 0x61,    // Clock1 Output Configuration (Table 109)
	CLK2_OUT_CNFIG_REG1   = 0x62,    // Clock2 Output Configuration (Table 110)
	CLK2_OUT_CNFIG_REG2   = 0x63,    // Clock2 Output Configuration (Table 111)
	CLK3_OUT_CNFIG_REG1   = 0x64,    // Clock3 Output Configuration (Table 112)
	CLK3_OUT_CNFIG_REG2   = 0x65,    // Clock3 Output Configuration (Table 113)
	CLK4_OUT_CNFIG_REG1   = 0x66,    // Clock4 Output Configuration (Table 114)
	CLK4_OUT_CNFIG_REG2   = 0x67     // Clock4 Output Configuration (Table 115)
};

enum MASK_INDEX
{
	// The names reference the number position that will be changed
	MASK_ONE        = 0xFE,  // 1111 1110
	MASK_TWO        = 0xFD,  // 1111 1101
	MASK_THREE      = 0xFC,  // 1111 1100
	MASK_FOUR       = 0xFB,  // 1111 1011
	MASK_FIVE       = 0xFA,  // 1111 1010
	MASK_SIX        = 0xF9,  // 1111 1001
	MASK_SEVEN      = 0xF8,  // 1111 1000
	MASK_EIGHT      = 0xF7,  // 1111 0111
	MASK_NINE       = 0xF6,  // 1111 0110
	MASK_TEN        = 0xF5,  // 1111 0101
	MASK_ELEVEN     = 0xF4,  // 1111 0100
	MASK_TWELVE     = 0xF3,  // 1111 0011
	MASK_THIRT      = 0xF2,  // 1111 0010
	MASK_FOURT      = 0xF1,  // 1111 0001
	MASK_FIFT       = 0xF0,  // 1111 0000

	MASK_FIFT_MSB   = 0x0F,  // 0000 1111
	MASK_THIRT_MSB  = 0x1F,  // 0001 1111
	MASK_EIGHT_MSB  = 0x7F,  // 0111 1111
	MASK_FOUR_MSB   = 0xBF,  // 1011 1111
	MASK_THR_MSB    = 0x8F,  // 1000 1111
	MASK_TWO_MSB    = 0xDF,  // 1101 1111
	MASK_ONE_MSB    = 0xEF,  // 1110 1111

	MASK_ALL        = 0x00,      // 0000 0000
	MASK_ALL_8_BIT  = 0x0000FF,  // 0000 0000 0000 0000 1111 1111
	MASK_ALL_12_BIT = 0xFF0,     // 1111 1111 0000
	MASK_ALL_16_BIT = 0x00FFFF,  // 0000 0000 1111 1111 1111 1111
	MASK_ALL_24_BIT = 0xFF0000,  // 1111 1111 0000 0000 0000 0000

	MASK_ENDS       = 0x81  // 1000 0001
};

enum BIT_POS_INDEX
{
	POS_ZERO  = 0x00,
	POS_ONE   = 0x01,
	POS_TWO   = 0x02,
	POS_THREE = 0x03,
	POS_FOUR  = 0x04,
	POS_FIVE  = 0x05,
	POS_SIX   = 0x06,
	POS_SEVEN = 0x07,
	POS_EIGHT = 0x08,
	POS_FIFT  = 0x0F,
	POS_SIXT  = 0x10
};

class SparkFun_5P49V60
{
	public:
		SparkFun_5P49V60(CLOCKGEN_I2C_ADDRESS address = I2C_DEFAULT, uint8_t freq = DEF_CLOCK);

		bool     begin(TwoWire &wirePort = Wire);

		void     addCrystalLoadCap(uint8_t, uint8_t);
		void     auxControl(CLOCK_CHANNEL, bool);
		void     bypassPllFilterThree(bool);
		void     bypassRefDivider(bool);
		void     bypassThirdFilter(bool);
		void     calibrateVco();
		void     changeI2CAddress(CLOCKGEN_I2C_ADDRESS);
		void     changeSource(CLOCKGEN_SOURCE);
		void     clockConfigMode(CLOCK_CHANNEL, CLOCK_MODE);
		void     clockControl(CLOCK_CHANNEL, bool);
		void     clockInControl(bool);
		void     doubleRefFreqControl(bool);
		void     globalReset();
		void     globalSdControl(uint8_t);
		void     integerMode(CLOCK_CHANNEL, bool);
		void     muxFodOneToFodTwo();
		void     muxOutOneToOutTwo();
		void     muxOutTwoToFodThree();
		void     muxOutTwoToOutThree();
		void     muxOutThreeToFodFour();
		void     muxOutThreeToOutFour();
		void     muxPlltoFod(CLOCK_CHANNEL, bool);
		void     muxRefClockToOutOne();
		void     muxRefClockToFodOne();
		void     persEnableClock(CLOCK_CHANNEL);
		uint8_t  readBurnedBit();
		uint8_t  readBypassDivider();
		float    readCrystalCapVal(uint8_t);
		uint8_t  readI2CAddress();
		uint32_t readFractDivFod(CLOCK_CHANNEL);
		float    readFractDivSkew(CLOCK_CHANNEL);
		uint16_t readIntDivOut(CLOCK_CHANNEL);
		uint16_t readIntDivSkew(CLOCK_CHANNEL);
		float    readPllFeedBackFractDiv();
		float    readPllFeedBackIntDiv();
		uint8_t  readPllFilterCapOne();
		float    readPllFilterCapTwo();
		uint16_t readPllFilterResOne();
		uint16_t readPllFilterResTwo();
		uint8_t  readRefDivider();
		uint8_t  readSource();
		uint8_t  readTestControl();
		void     refModeControl(bool);
		void     resetClockOne();
		void     resetClockTwo();
		void     resetFod(CLOCK_CHANNEL);
		void     sdInputPinControl(bool);
		void     selectRefDivider(uint8_t);
		void     setClockFreq(CLOCK_CHANNEL, float);
		void     setClockSlewRate(CLOCK_CHANNEL, CLOCK_SLEWRATE);
		void     setClockVoltage(CLOCK_CHANNEL, CLOCK_VOLTAGE);
		void     setFractDivFod(CLOCK_CHANNEL, uint32_t);
		void     setFractDivSkew(CLOCK_CHANNEL, float);
		void     setIntDivOut(CLOCK_CHANNEL, uint8_t);
		void     setIntDivSkew(CLOCK_CHANNEL, uint8_t);
		void     setPllFeedBackFractDiv(uint32_t);
		void     setPllFeedbackIntDiv(uint16_t);
		void     setPllFilterCapOne(uint8_t);
		void     setPllFilterCapTwo(float);
		void     setPllFilterChargePump(uint8_t);
		void     setPllFilterResOne(uint16_t);
		void     setPllFilterResTwo(uint16_t);
		void     setSigmaDeltaMod(uint8_t);
		void     setVcoFrequency(float);
		void     skewClock(CLOCK_CHANNEL, float);
		void     vcoTestControl(bool);
		void     xtalControl(bool);

	private:
		uint8_t   _address;
		uint8_t   _clock_freq;
		uint16_t  _vco_freq;
		TwoWire  *_i2cPort;

		float    _calculate_skew_frac(CLOCK_CHANNEL, float);
		uint8_t  _calculate_skew_int(CLOCK_CHANNEL, float);
		float    _calculate_skew_value(CLOCK_CHANNEL, float);
		uint8_t  _readRegister(uint8_t);
		void     _writeRegister(uint8_t, uint8_t, uint8_t, uint8_t);
};
#endif

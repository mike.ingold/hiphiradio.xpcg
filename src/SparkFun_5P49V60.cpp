/*
Notice: this code is forked from a library developed by SparkFun and carries the following license:

The MIT License (MIT)

Copyright (c) 2016 SparkFun Electronics, Elias Santistevan

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include "SparkFun_5P49V60.h"

SparkFun_5P49V60::SparkFun_5P49V60(CLOCKGEN_I2C_ADDRESS address, uint8_t freq)
{
	switch (address) {
	case I2C_DEFAULT:
		_address = 0x6A; break;
	case I2C_ALTERNATE:
		_address = 0x68; break;
	}

	_clock_freq = freq;
}

bool     SparkFun_5P49V60::begin(TwoWire &wirePort)
{
	uint8_t ret_code;

	_i2cPort = &wirePort;
	_i2cPort->beginTransmission(_address);
	delay(100); // 100ms Startup time
	ret_code = _i2cPort->endTransmission();

	if (!ret_code)
		return true;
	else
		return false;
}

// Adds the available internal capacitors to the given pin on the crystal. The two available pins can be taken directly
// from the datasheet or schematic: XIN and XOUT.
// TODO figure out uint cap_val. Probably _cap_arr[i]
void     SparkFun_5P49V60::addCrystalLoadCap(uint8_t pin, uint8_t cap_val)
{
	// Translate specified pin into corresponding register address
	uint8_t reg;
	switch (pin) {
	case XIN:
		reg = LOAD_CAP_REG1; break;
	case XOUT:
		reg = LOAD_CAP_REG2; break;
	default:
		return; // Undefined input
	}

	// Translate specified capacitance value into register bit position and mask
	uint8_t bit_pos;
	uint8_t mask;
	switch (cap_val) {
	case 1:
		bit_pos = POS_TWO;
		mask = MASK_FOUR;
		break;
	case 2:
		bit_pos = POS_THREE;
		mask = MASK_EIGHT;
		break;
	case 3:
		bit_pos = POS_FOUR;
		mask = MASK_ONE_MSB;
		break;
	case 4:
		bit_pos = POS_FIVE;
		mask = MASK_TWO_MSB;
		break;
	case 5:
		bit_pos = POS_SIX;
		mask = MASK_FOUR_MSB;
		break;
	case 6:
		bit_pos = POS_SEVEN;
		mask = MASK_EIGHT_MSB;
		break;
	default:
		return; // Undefined input
	}

	_writeRegister(reg, mask, cap_val, bit_pos);
}

void     SparkFun_5P49V60::auxControl(CLOCK_CHANNEL channel, bool control)
{
	// Translate specified channel into corresponding register address
	REGISTER_INDEX reg;
	switch (channel) {
	case CHANNEL_0:
		return; // Operation undefined
	case CHANNEL_1:
		reg = OUT1_ISKEW_REG2; break;
	case CHANNEL_2:
		reg = OUT2_ISKEW_REG2; break;
	case CHANNEL_3:
		reg = OUT3_ISKEW_REG2; break;
	case CHANNEL_4:
		reg = OUT4_ISKEW_REG2; break;
	}

	if ( control )
		_writeRegister(reg, MASK_ONE, ENABLE, POS_ZERO);
	else
		_writeRegister(reg, MASK_ONE, DISABLE, POS_ZERO);
}

void     SparkFun_5P49V60::bypassPllFilterThree(bool bypass)
{
	if ( bypass )
		_writeRegister(RC_CONTR_REG3, MASK_EIGHT_MSB, ENABLE, POS_SEVEN);
	else
		_writeRegister(RC_CONTR_REG3, MASK_EIGHT_MSB, DISABLE, POS_SEVEN);
}

// Enables the function to bypass the clock pre-divider, which allows for greater division of the ref clock
void     SparkFun_5P49V60::bypassRefDivider(bool control)
{
	if ( control )
		_writeRegister(DIVIDER_VCO_REG, MASK_EIGHT_MSB, ENABLE, POS_SEVEN);
	else
		_writeRegister(DIVIDER_VCO_REG, MASK_EIGHT_MSB, ENABLE, POS_SEVEN);
}

void     SparkFun_5P49V60::bypassThirdFilter(bool control)
{
	if ( control )
		_writeRegister(RC_CONTR_REG3, MASK_FOUR_MSB, ENABLE, POS_SEVEN);
	else
		_writeRegister(RC_CONTR_REG3, MASK_FOUR_MSB, DISABLE, POS_SEVEN);
}

// Forces the VCO band to manually calibrate
void     SparkFun_5P49V60::calibrateVco()
{
	_writeRegister(VC_CONTROL_REG, MASK_EIGHT_MSB, DISABLE, POS_SEVEN);
	delay(1);
	_writeRegister(VC_CONTROL_REG, MASK_EIGHT_MSB, ENABLE, POS_SEVEN);
}

// TODO change to enumerated PRIMARY/ALTERNATE I2C address
void     SparkFun_5P49V60::changeI2CAddress(CLOCKGEN_I2C_ADDRESS address)
{
	uint8_t bits;
	switch (address) {
	case I2C_DEFAULT:
		bits = 0x01; break;
	case I2C_ALTERNATE:
		bits = 0x00; break;
	}

	_writeRegister(OTP_CONTROL_REG, MASK_ONE, bits, POS_ZERO);
}

// Change the primary source of the clock: crystal or CLKIN. The product is set by default in hardware to use the xtal
void     SparkFun_5P49V60::changeSource(CLOCKGEN_SOURCE source)
{
	uint8_t bits;
	switch (source) {
	case CLOCK_SOURCE:
		bits = 0x00; break;
	case XTAL_SOURCE:
		bits = 0x01; break;
	}

	_writeRegister(LOAD_CAP_REG2, MASK_THREE, bits, POS_ONE);
}

// Set the clock output mode for a particular channel
void     SparkFun_5P49V60::clockConfigMode(CLOCK_CHANNEL channel, CLOCK_MODE mode)
{
	uint8_t bits;
	switch (mode) {
	case LVPECL_MODE:
		bits = 0x00; break;
	case CMOS_MODE:
		bits = 0x01; break;
	case HCSL33_MODE:
		bits = 0x02; break;
	case LVDS_MODE:
		bits = 0x03; break;
	case CMOS2_MODE:
		bits = 0x04; break;
	case CMOSD_MODE:
		bits = 0x05; break;
	case HCSL25_MODE:
		bits = 0x06; break;
	}

	REGISTER_INDEX reg;
	switch (channel) {
	case CHANNEL_0:
		return; // Operation undefined
	case CHANNEL_1:
		reg = CLK1_OUT_CNFIG_REG1; break;
	case CHANNEL_2:
		reg = CLK2_OUT_CNFIG_REG1; break;
	case CHANNEL_3:
		reg = CLK3_OUT_CNFIG_REG1; break;
	case CHANNEL_4:
		reg = CLK4_OUT_CNFIG_REG1; break;
	}

	_writeRegister(reg, MASK_THIRT_MSB, bits, POS_FIVE);
}

// Enable clock output on a channel
void     SparkFun_5P49V60::clockControl(CLOCK_CHANNEL channel, bool control)
{
	REGISTER_INDEX reg;
	switch (channel) {
	case CHANNEL_0:
		return; // Operation undefined
	case CHANNEL_1:
		reg = CLK1_OUT_CNFIG_REG2; break;
	case CHANNEL_2:
		reg = CLK2_OUT_CNFIG_REG2; break;
	case CHANNEL_3:
		reg = CLK3_OUT_CNFIG_REG2; break;
	case CHANNEL_4:
		reg = CLK4_OUT_CNFIG_REG2; break;
	}

	if ( control )
		_writeRegister(reg, MASK_ONE, ENABLE, POS_ZERO);
	else
		_writeRegister(reg, MASK_ONE, DISABLE, POS_ZERO);
}

void     SparkFun_5P49V60::clockInControl(bool control)
{
	if ( control ) {
		_writeRegister(SHUTDOWN_REG, MASK_FOUR_MSB, ENABLE, POS_SIX);
		xtalControl(DISABLE);
	} else {
		_writeRegister(SHUTDOWN_REG, MASK_FOUR_MSB, DISABLE, POS_SIX);
	}
}

void     SparkFun_5P49V60::doubleRefFreqControl(bool control)
{
	if ( control )
		_writeRegister(SHUTDOWN_REG, MASK_EIGHT, ENABLE, POS_THREE);
	else
		_writeRegister(SHUTDOWN_REG, MASK_EIGHT, DISABLE, POS_THREE);
}

// This register is not listed in the datasheet officially but is briefly mentioned on page 49 re: enabling skew.
void     SparkFun_5P49V60::globalReset()
{
	_writeRegister(GLOBAL_RESET_REG, MASK_TWO_MSB, DISABLE, POS_FIVE);
	delay(1);
	_writeRegister(GLOBAL_RESET_REG, MASK_TWO_MSB, ENABLE, POS_FIVE);
}

void     SparkFun_5P49V60::globalSdControl(uint8_t control)
{
	if ( control )
		_writeRegister(SHUTDOWN_REG, MASK_ONE, ENABLE, POS_ZERO);
	else
		_writeRegister(SHUTDOWN_REG, MASK_ONE, DISABLE, POS_ZERO);
}

void     SparkFun_5P49V60::integerMode(CLOCK_CHANNEL channel, bool control)
{
	REGISTER_INDEX reg;
	switch (channel) {
	case CHANNEL_0:
		return; // Operation undefined
	case CHANNEL_1:
		reg = DIV1_CONTROL_REG; break;
	case CHANNEL_2:
		reg = DIV2_CONTROL_REG; break;
	case CHANNEL_3:
		reg = DIV3_CONTROL_REG; break;
	case CHANNEL_4:
		reg = DIV4_CONTROL_REG; break;
	}

	if ( control )
		_writeRegister(reg, MASK_TWO, ENABLE, POS_ONE);
	else
		_writeRegister(reg, MASK_TWO, ENABLE, POS_ONE);
}

/// Multiplex the output from FOD1 to the input of FOD2.
void     SparkFun_5P49V60::muxFodOneToFodTwo()
{
	auxControl(CHANNEL_1, ENABLE);
	_writeRegister(DIV2_CONTROL_REG, MASK_FIFT, 0x0F, POS_ZERO);
	resetFod(CHANNEL_2);
}

void     SparkFun_5P49V60::muxOutOneToOutTwo()
{
	auxControl(CHANNEL_1, ENABLE);
	_writeRegister(DIV2_CONTROL_REG, MASK_FIFT, 0x03, POS_TWO);
	resetFod(CHANNEL_2);
}

void     SparkFun_5P49V60::muxOutTwoToFodThree()
{
	auxControl(CHANNEL_2, ENABLE);
	_writeRegister(DIV3_CONTROL_REG, MASK_FIFT, 0x0F, POS_ZERO);
	resetFod(CHANNEL_3);
}

void     SparkFun_5P49V60::muxOutTwoToOutThree()
{
	auxControl(CHANNEL_2, ENABLE);
	_writeRegister(DIV3_CONTROL_REG, MASK_FIFT, 0x03, POS_TWO);
	resetFod(CHANNEL_3);
}

void     SparkFun_5P49V60::muxOutThreeToFodFour()
{
	auxControl(CHANNEL_3, ENABLE);
	_writeRegister(DIV4_CONTROL_REG, MASK_FIFT, 0x0F, POS_ZERO);
	resetFod(CHANNEL_4);
}

void     SparkFun_5P49V60::muxOutThreeToOutFour()
{
	auxControl(CHANNEL_3, ENABLE);
	_writeRegister(DIV4_CONTROL_REG, MASK_FIFT, 0x03, POS_TWO);
	resetFod(CHANNEL_4);
}

/// Multiplex the PLL/VCO output signal to the FOD for a particular CHANNEL.
/// @param(channel)  Designated FOD channel number
/// @param(control)  Activate (true) or deactivate (false)
void     SparkFun_5P49V60::muxPlltoFod(CLOCK_CHANNEL channel, bool control)
{
	REGISTER_INDEX reg;
	switch (channel) {
	case CHANNEL_0:
		return; // Operation undefined
	case CHANNEL_1:
		reg = DIV1_CONTROL_REG; break;
	case CHANNEL_2:
		reg = DIV2_CONTROL_REG; break;
	case CHANNEL_3:
		reg = DIV3_CONTROL_REG; break;
	case CHANNEL_4:
		reg = DIV4_CONTROL_REG; break;
	}

	if ( control )
		_writeRegister(reg, MASK_THIRT, ENABLE, POS_ZERO);
	else
		_writeRegister(reg, MASK_THIRT, DISABLE, POS_ZERO);

	resetFod(channel);
}

/// Multiplex reference clock signal (CHANNEL_0) to FOD for CHANNEL_1.
void     SparkFun_5P49V60::muxRefClockToFodOne()
{
	refModeControl(ENABLE);
	_writeRegister(DIV1_CONTROL_REG, MASK_FIFT, 0x0F, POS_ZERO);
	resetFod(CHANNEL_1);
}

/// Multiplex reference clock signal (CHANNEL_0) to CHANNEL_1.
void     SparkFun_5P49V60::muxRefClockToOutOne()
{
	refModeControl(ENABLE);
	_writeRegister(DIV1_CONTROL_REG, MASK_TWO, 0xF0, POS_ZERO);
	resetFod(CHANNEL_1);
}

// Sets the Output Enable Bit that controls behavior of Output lines (1-4) when the SD/OE line is set to OUTPUT ENABLE.
// Example: Ch1 & Ch2 set to 1, Ch3 & Ch4 set to 0. When SD/OE is LOW (ON) then Ch1 & Ch2 are ON, Ch3 & Ch4 are OFF.
void     SparkFun_5P49V60::persEnableClock(CLOCK_CHANNEL channel)
{
	BIT_POS_INDEX bit_pos;
	uint8_t bits;
	MASK_INDEX mask;
	switch (channel) {
	case CHANNEL_0:
		bit_pos = POS_SEVEN;
		bits = 0x00;
		mask = MASK_EIGHT_MSB;
		break;
	case CHANNEL_1:
		bit_pos = POS_SIX;
		bits = 0x01;
		mask = MASK_FOUR_MSB;
		break;
	case CHANNEL_2:
		bit_pos = POS_FIVE;
		bits = 0x02;
		mask = MASK_TWO_MSB;
		break;
	case CHANNEL_3:
		bit_pos = POS_FOUR;
		bits = 0x03;
		mask = MASK_ONE_MSB;
		break;
	case CHANNEL_4:
		bit_pos = POS_THREE;
		bits = 0x04;
		mask = MASK_FOUR;
		break;
	}

	// TODO verify behavior of the following; bits vs ENABLE/DISABLE/0xFF
	_writeRegister(CLK_OE_FUNC_REG, mask, bits, bit_pos);
}

uint8_t  SparkFun_5P49V60::readBurnedBit()
{
	uint8_t reg_val;
	reg_val = _readRegister(OTP_CONTROL_REG);
	reg_val &= ~MASK_EIGHT_MSB;
	reg_val >>= POS_SEVEN;
	return reg_val;
}

// Reads whether the clock pre-divider is being bypassed
uint8_t  SparkFun_5P49V60::readBypassDivider()
{
	uint8_t reg_val;
	reg_val = _readRegister(DIVIDER_VCO_REG);
	reg_val &= (~MASK_EIGHT_MSB);
	reg_val >>= POS_SEVEN;
	return reg_val;
}

// (INCOMPLETE) Returns the total load capacitance on the given pin of the onboard crystal
float    SparkFun_5P49V60::readCrystalCapVal(uint8_t pin)
{
	// Translate specified pin to corresponding register address
	REGISTER_INDEX reg;
	switch (pin) {
	case XIN:
		reg = LOAD_CAP_REG1;
		break;
	case XOUT:
		reg = LOAD_CAP_REG2;
		break;
	default:
		return UNKNOWN_ERROR_F;
	}

	// Read capacitor index value from register
	uint8_t reg_val;
	reg_val = _readRegister(reg);
	reg_val &= MASK_THREE;

	// Translate register index value to capacitor value using LUT
	switch (reg_val) {
	case 1:
		return _cap_arr[0];
	case 2:
		return _cap_arr[1];
	case 3:
		return ( _cap_arr[0] + _cap_arr[1] );
	case 4:
		return _cap_arr[2];
	case 5:
		return ( _cap_arr[0] + _cap_arr[2] );
	case 6:
		return ( _cap_arr[1] + _cap_arr[2] );
	default:
		return UNKNOWN_ERROR_F;
	}
}

uint32_t SparkFun_5P49V60::readFractDivFod(CLOCK_CHANNEL channel)
{
	REGISTER_INDEX reg1, reg2, reg3, reg4;
	switch (channel) {
	case CHANNEL_0:
		return ERROR_32B; // Operation undefined
	case CHANNEL_1:
		reg1 = OUT1_FDIV_REG1;
		reg2 = OUT1_FDIV_REG2;
		reg3 = OUT1_FDIV_REG3;
		reg4 = OUT1_FDIV_REG4;
		break;
	case CHANNEL_2:
		reg1 = OUT2_FDIV_REG1;
		reg2 = OUT2_FDIV_REG2;
		reg3 = OUT2_FDIV_REG3;
		reg4 = OUT2_FDIV_REG4;
		break;
	case CHANNEL_3:
		reg1 = OUT3_FDIV_REG1;
		reg2 = OUT3_FDIV_REG2;
		reg3 = OUT3_FDIV_REG3;
		reg4 = OUT3_FDIV_REG4;
		break;
	case CHANNEL_4:
		reg1 = OUT4_FDIV_REG1;
		reg2 = OUT4_FDIV_REG2;
		reg3 = OUT4_FDIV_REG3;
		reg4 = OUT4_FDIV_REG4;
		break;
	}

	uint32_t llsb_div_val = _readRegister(reg4) >> POS_TWO;
	uint32_t lsb_div_val  = _readRegister(reg3);
	uint32_t msb_div_val  = _readRegister(reg2);
	uint32_t mmsb_div_val = _readRegister(reg1);

	uint32_t ret_val;
	ret_val = llsb_div_val;
	ret_val |= ( lsb_div_val  << POS_SIX );
	ret_val |= ( msb_div_val  << ( POS_SIX + 8 ) );
	ret_val |= ( mmsb_div_val << ( POS_SIX + 16 ) );

	return ret_val;
}

float    SparkFun_5P49V60::readFractDivSkew(CLOCK_CHANNEL channel)
{
	REGISTER_INDEX reg;
	switch (channel) {
	case CHANNEL_0:
		return UNKNOWN_ERROR_F; // Operation undefined
	case CHANNEL_1:
		reg = OUT1_FSKEW_REG; break;
	case CHANNEL_2:
		reg = OUT2_FSKEW_REG; break;
	case CHANNEL_3:
		reg = OUT3_FSKEW_REG; break;
	case CHANNEL_4:
		reg = OUT4_FSKEW_REG; break;
	}

	uint8_t reg_val = _readRegister(reg);
	float f_reg_val = static_cast<float>(reg_val);
	f_reg_val = f_reg_val / pow(2.0, 6.0);

	return f_reg_val;
}

/// Get the I2C Address being used by this Clock Generator
/// @return The I2C Address in use: 0xD0 or 0xD4
uint8_t  SparkFun_5P49V60::readI2CAddress()
{
	// Read the register byte storing the I2C Address setting bit, extract the specific bit of interest
	uint8_t reg_val;
	reg_val = _readRegister(OTP_CONTROL_REG);
	reg_val &= (~MASK_ONE);

	// Translate the single bit setting value to corresponding I2C address setting
	switch (reg_val) {
	case 0x00:
		return 0xD0;
	case 0x01:
		return 0xD4;
	default:
		return ERROR;
	}
}

uint16_t SparkFun_5P49V60::readIntDivOut(CLOCK_CHANNEL channel)
{
	REGISTER_INDEX reg1, reg2;
	switch (channel) {
	case CHANNEL_0:
		return ERROR_16B; // Operation undefined
	case CHANNEL_1:
		reg1 = OUT1_IDIV_REG1;
		reg2 = OUT1_IDIV_REG2;
		break;
	case CHANNEL_2:
		reg1 = OUT2_IDIV_REG1;
		reg2 = OUT2_IDIV_REG2;
		break;
	case CHANNEL_3:
		reg1 = OUT3_IDIV_REG1;
		reg2 = OUT3_IDIV_REG2;
		break;
	case CHANNEL_4:
		reg1 = OUT4_IDIV_REG1;
		reg2 = OUT4_IDIV_REG2;
		break;
	}

	uint8_t lsb_div_val;
	uint8_t msb_div_val;
	uint16_t ret_val;

	lsb_div_val = _readRegister(reg2) >> POS_FOUR;
	msb_div_val = _readRegister(reg1);
	ret_val     = uint16_t(msb_div_val) << 3;
	ret_val |= lsb_div_val;

	return ret_val;
}

uint16_t SparkFun_5P49V60::readIntDivSkew(CLOCK_CHANNEL channel)
{
	REGISTER_INDEX reg1, reg2;
	switch (channel) {
	case CHANNEL_0:
		return ERROR_16B; // Operation undefined
	case CHANNEL_1:
		reg1 = OUT1_ISKEW_REG1;
		reg2 = OUT1_ISKEW_REG2;
		break;
	case CHANNEL_2:
		reg1 = OUT2_ISKEW_REG1;
		reg2 = OUT2_ISKEW_REG2;
		break;
	case CHANNEL_3:
		reg1 = OUT3_ISKEW_REG1;
		reg2 = OUT3_ISKEW_REG2;
		break;
	case CHANNEL_4:
		reg1 = OUT4_ISKEW_REG1;
		reg2 = OUT4_ISKEW_REG2;
		break;
	}

	uint8_t lsb_div_val;
	uint8_t msb_div_val;
	uint16_t ret_val;

	lsb_div_val = _readRegister(reg2) >> POS_FOUR;
	msb_div_val = _readRegister(reg1);
	ret_val     = uint16_t(msb_div_val) << 3;
	ret_val |= lsb_div_val;

	return ret_val;
}

float SparkFun_5P49V60::readPllFeedBackFractDiv()
{
	uint32_t lsb_div_val;
	uint32_t msb_div_val;
	uint32_t mmsb_div_val;
	float ret_val;

	// Read three 8-bit registers, combine into a single 32-bit value
	lsb_div_val  = _readRegister(FDB_FRAC_DIV_REG3);
	msb_div_val  = _readRegister(FDB_FRAC_DIV_REG2) << POS_EIGHT;
	mmsb_div_val = _readRegister(FDB_FRAC_DIV_REG1) << POS_SIXT;
	mmsb_div_val |= msb_div_val;
	mmsb_div_val |= lsb_div_val;
	ret_val = static_cast<float>(mmsb_div_val);

	return ret_val;
}

// Reads the Feedback Integer Divider Value
float SparkFun_5P49V60::readPllFeedBackIntDiv()
{
	uint8_t lsb_div_val;
	uint8_t msb_div_val;
	uint16_t ret_val;
	float f_ret_val;

	// Read two 8-bit registers, combine into a single 16-bit value
	lsb_div_val = _readRegister(FDB_INT_DIV_REG2) >> POS_FOUR;
	msb_div_val = _readRegister(FDB_INT_DIV_REG1);
	ret_val     =  uint16_t(msb_div_val) << 3;
	ret_val    |=  lsb_div_val;
	f_ret_val   = static_cast<float>(ret_val);

	return f_ret_val;
}

uint8_t  SparkFun_5P49V60::readPllFilterCapOne()
{
	uint8_t cap_val = _readRegister(RC_CONTR_REG2);
	cap_val &= (~MASK_SEVEN);

	if ( cap_val == 0b000 )
		return 12;
	else if ( cap_val == 0b001 )
		return 16;
	else if ( cap_val == 0b010 )
		return 20;
	else if ( cap_val == 0b011 )
		return 24;
	else if ( cap_val == 0b100 )
		return 28;
	else
		return UNKNOWN_ERROR;
}

float    SparkFun_5P49V60::readPllFilterCapTwo()
{
	uint8_t cap_val = _readRegister(RC_CONTR_REG3);
	cap_val &= (~MASK_THR_MSB);
	cap_val >>= POS_FOUR;

	if ( cap_val == 0b001 )
		return 1.8;
	else if ( cap_val== 0b011 )
		return 3.6;
	else if ( cap_val == 0b111 )
		return 5.4;
	else
		return UNKNOWN_ERROR_F;
}

uint16_t SparkFun_5P49V60::readPllFilterResOne()
{
	uint8_t res_val = _readRegister(RC_CONTR_REG2);
	res_val &= MASK_SEVEN;
	res_val >>= POS_THREE;

	if ( res_val == 0b11110 )
		return 1500;
	else if ( res_val == 0 )
		return 46500;
	else
		return UNKNOWN_ERROR;
}

uint16_t SparkFun_5P49V60::readPllFilterResTwo()
{
	uint8_t res_val = _readRegister(RC_CONTR_REG3);
	res_val &= (~MASK_FOURT);

	// Values in Ohms
	if ( res_val == 0b100 )
		return 1000;
	else if ( res_val == 0b111 )
		return 1450;
	else if ( res_val == 0b011 )
		return 1600;
	else if ( res_val == 0b001 )
		return 2000;
	else if ( res_val == 0b110 )
		return 5300;
	else if ( res_val == 0b101 )
		return 7000;
	else if ( res_val == 0b010 )
		return 8000;
	else
		return UNKNOWN_ERROR;
}

// Select which divider value is used for the clock output
uint8_t  SparkFun_5P49V60::readRefDivider()
{
	uint8_t reg_val;
	reg_val = _readRegister(REF_DIVIDER_REG);

	// Divider 2 disables other divider.
	if ( ((reg_val & (~MASK_EIGHT_MSB)) >> POS_SEVEN) == 1 )
		return 2;
	else
		return reg_val &= MASK_EIGHT_MSB;
}

// Returns the currently selected source of the clock: xtal or CLKIN.
// The SparkFun devboard is set by default in hardware to use the xtal
uint8_t  SparkFun_5P49V60::readSource()
{
	uint8_t reg_val;
	reg_val = _readRegister(LOAD_CAP_REG2);
	reg_val &= (~MASK_TWO);
	reg_val >>= POS_ONE;
	return reg_val;
}

// Check to see if the VCO test mode is running
uint8_t  SparkFun_5P49V60::readTestControl()
{
	uint8_t reg_val;
	reg_val = _readRegister(VCO_BAND_REG);
	reg_val &= (~MASK_TWO);
	reg_val >>= POS_FIVE;
	return reg_val;
}

void     SparkFun_5P49V60::refModeControl(bool control)
{
	if ( control )
		_writeRegister(SHUTDOWN_REG, MASK_FOUR, ENABLE, POS_TWO);
	else
		_writeRegister(SHUTDOWN_REG, MASK_FOUR, DISABLE, POS_TWO);
}

// Disables the clock before enabling it again
void     SparkFun_5P49V60::resetClockOne()
{
	clockControl(CHANNEL_1, DISABLE);
	delay(1);
	clockControl(CHANNEL_1, ENABLE);
}

// Disables the clock before enabling it again
void     SparkFun_5P49V60::resetClockTwo()
{
	clockControl(CHANNEL_2, DISABLE);
	delay(1);
	clockControl(CHANNEL_2, ENABLE);
}

void     SparkFun_5P49V60::resetFod(CLOCK_CHANNEL channel)
{
	REGISTER_INDEX reg;
	switch (channel) {
	case CHANNEL_0:
		return; // Operation undefined
	case CHANNEL_1:
		reg = DIV1_CONTROL_REG; break;
	case CHANNEL_2:
		reg = DIV2_CONTROL_REG; break;
	case CHANNEL_3:
		reg = DIV3_CONTROL_REG; break;
	case CHANNEL_4:
		reg = DIV4_CONTROL_REG; break;
	}

	_writeRegister(reg, MASK_FOUR_MSB, DISABLE, POS_SEVEN);
	delay(5);
	_writeRegister(reg, MASK_FOUR_MSB, ENABLE, POS_SEVEN);
}

void     SparkFun_5P49V60::sdInputPinControl(bool control)
{
	if ( control )
		_writeRegister(SHUTDOWN_REG, MASK_TWO, ENABLE, POS_ONE);
	else
		_writeRegister(SHUTDOWN_REG, MASK_TWO, ENABLE, POS_ONE);
}

// Set divider value for the clock output. This is strange, but the two divider is in the 8th bit position, and values
// of 3-127 are written to bits[6:0]. If the two divider is enabled then the other divider is disabled - this bit can
// not be set and then the others as well.
void     SparkFun_5P49V60::selectRefDivider(uint8_t div_val)
{
	if ( div_val == 2 ) {
		_writeRegister(REF_DIVIDER_REG, MASK_EIGHT_MSB, ENABLE, POS_SEVEN);
	} else if ( ( div_val > 2 ) && ( div_val <= 127 ) )
		_writeRegister(REF_DIVIDER_REG, (~MASK_EIGHT_MSB), div_val, POS_ZERO);
}

/// Set the clock frequency for a particular output channel.
/// @param(channel)  Output channel whose frequency to set
/// @param(freq)  Desired clock frequency
void     SparkFun_5P49V60::setClockFreq(CLOCK_CHANNEL channel, float freq)
{
	float division = (_vco_freq/2)/freq;
	uint32_t int_portion = static_cast<uint32_t>(division);
	float decimal = fmod(division, int_portion);
	uint32_t frac_portion = static_cast<uint32_t>(decimal/pow(2.0,24.0));

	setIntDivOut(channel, int_portion);
	setFractDivFod(channel, frac_portion);
	clockControl(channel, ENABLE);
}

/// Set the signal slew rate for a particular clock output channel
/// @param(channel)  Output channel whose slew rate to set
/// @param(rate)  Slew rate to set
void     SparkFun_5P49V60::setClockSlewRate(CLOCK_CHANNEL channel, CLOCK_SLEWRATE rate)
{
	uint8_t bits;
	switch (rate) {
	case SLOWEST:
		bits = 0x00; break;
	case SLOW:
		bits = 0x01; break;
	case FAST:
		bits = 0x02; break;
	case FASTEST:
		bits = 0x03; break;
	}

	REGISTER_INDEX reg;
	switch (channel) {
	case CHANNEL_0:
		_writeRegister(CLK_OE_FUNC_REG, MASK_FOUR, bits, POS_TWO);
		return;
	case CHANNEL_1:
		reg = CLK1_OUT_CNFIG_REG1; break;
	case CHANNEL_2:
		reg = CLK2_OUT_CNFIG_REG1; break;
	case CHANNEL_3:
		reg = CLK3_OUT_CNFIG_REG1; break;
	case CHANNEL_4:
		reg = CLK4_OUT_CNFIG_REG1; break;
	}

	_writeRegister(reg, MASK_THREE, bits, POS_ZERO);
}

/// Set the clock output voltage for a particular channel
/// @param(channel)  Output channel whose voltage to set
/// @param(voltage)  Voltage level to set
void     SparkFun_5P49V60::setClockVoltage(CLOCK_CHANNEL channel, CLOCK_VOLTAGE voltage)
{
	// Translate specified voltage into corresponding register value
	uint8_t bits;
	switch (voltage) {
	case ONE_EIGHT_V:
		bits = 0x00; break;
	case TWO_FIVE_V:
		bits = 0x02; break;
	case THREE_THREE_V:
		bits = 0x03; break;
	}

	// Translate specified channel into corresponding register address
	REGISTER_INDEX reg;
	switch (channel) {
	case CHANNEL_0:
		// Special case
		_writeRegister(CLK_OE_FUNC_REG, MASK_THREE, bits, POS_ZERO);
		return;
	case CHANNEL_1:
		reg = CLK1_OUT_CNFIG_REG1; break;
	case CHANNEL_2:
		reg = CLK2_OUT_CNFIG_REG1; break;
	case CHANNEL_3:
		reg = CLK3_OUT_CNFIG_REG1; break;
	case CHANNEL_4:
		reg = CLK4_OUT_CNFIG_REG1; break;
	}

	_writeRegister(reg, MASK_TWELVE, bits, POS_THREE);
}

void     SparkFun_5P49V60::setFractDivFod(CLOCK_CHANNEL channel, uint32_t divider_val)
{
	if ( divider_val > 1073741823 ) // max value 1,073,741,823
		return;

	REGISTER_INDEX reg1, reg2, reg3, reg4;
	switch (channel) {
	case CHANNEL_0:
		return; // Operation undefined
	case CHANNEL_1:
		reg1 = OUT1_FDIV_REG1;
		reg2 = OUT1_FDIV_REG2;
		reg3 = OUT1_FDIV_REG3;
		reg4 = OUT1_FDIV_REG4;
		break;
	case CHANNEL_2:
		reg1 = OUT2_FDIV_REG1;
		reg2 = OUT2_FDIV_REG2;
		reg3 = OUT2_FDIV_REG3;
		reg4 = OUT2_FDIV_REG4;
		break;
	case CHANNEL_3:
		reg1 = OUT3_FDIV_REG1;
		reg2 = OUT3_FDIV_REG2;
		reg3 = OUT3_FDIV_REG3;
		reg4 = OUT3_FDIV_REG4;
		break;
	case CHANNEL_4:
		reg1 = OUT4_FDIV_REG1;
		reg2 = OUT4_FDIV_REG2;
		reg3 = OUT4_FDIV_REG3;
		reg4 = OUT4_FDIV_REG4;
		break;
	}

	_writeRegister(reg4, MASK_ALL, (divider_val & 0x3F), POS_ZERO);
	_writeRegister(reg3, MASK_ALL, ((divider_val & 0x3FFF) >> POS_SIX), POS_ZERO);
	_writeRegister(reg2, MASK_ALL, ((divider_val & 0x3FFFFF) >> (POS_SIX + 8)), POS_ZERO);
	_writeRegister(reg1, MASK_ALL, ((divider_val & 0x3FFFFFFF) >> (POS_SIX + 16)), POS_ZERO);
}

void     SparkFun_5P49V60::setFractDivSkew(CLOCK_CHANNEL channel, float frac_value)
{
	// frac_value is supposed to be = something modulo 1
	if ( ( frac_value < 0 ) || ( frac_value >= 1 ) )
		return;

	REGISTER_INDEX reg;
	switch (channel) {
	case CHANNEL_0:
		return; // Operation undefined
	case CHANNEL_1:
		reg = OUT1_FSKEW_REG; break;
	case CHANNEL_2:
		reg = OUT2_FSKEW_REG; break;
	case CHANNEL_3:
		reg = OUT3_FSKEW_REG; break;
	case CHANNEL_4:
		reg = OUT4_FSKEW_REG; break;
	}

	// fractional component scaled to 6-bit integer space per pg. 48 of programming guide
	float f_scaled_value = frac_value * pow(2.0,6.0);
	uint8_t i_scaled_value = static_cast<uint8_t>(f_scaled_value);

	_writeRegister(reg, MASK_ALL, i_scaled_value, POS_ZERO);
}

void     SparkFun_5P49V60::setIntDivOut(CLOCK_CHANNEL channel, uint8_t divider_val)
{
	/* See GitLab Issue #3
	if ( divider_val > 4095 )
		return;
	*/

	REGISTER_INDEX reg1, reg2;
	switch (channel) {
	case CHANNEL_0:
		return; // Operation undefined
	case CHANNEL_1:
		reg1 = OUT1_IDIV_REG1;
		reg2 = OUT1_IDIV_REG2;
		break;
	case CHANNEL_2:
		reg1 = OUT2_IDIV_REG1;
		reg2 = OUT2_IDIV_REG2;
		break;
	case CHANNEL_3:
		reg1 = OUT3_IDIV_REG1;
		reg2 = OUT3_IDIV_REG2;
		break;
	case CHANNEL_4:
		reg1 = OUT4_IDIV_REG1;
		reg2 = OUT4_IDIV_REG2;
		break;
	}

	_writeRegister(reg2, MASK_FIFT_MSB, (divider_val & MASK_FIFT_MSB), POS_FOUR);
	_writeRegister(reg1, MASK_ALL, ((divider_val & MASK_ALL_12_BIT) >> POS_THREE), POS_ZERO);
}

void     SparkFun_5P49V60::setIntDivSkew(CLOCK_CHANNEL channel, uint8_t divider_val)
{
	/* See GitLab Issue #3
	if ( divider_val > 4095 )
		return;
	*/

	REGISTER_INDEX reg1, reg2;
	switch (channel) {
	case CHANNEL_0:
		return; // Operation undefined
	case CHANNEL_1:
		reg1 = OUT1_ISKEW_REG1;
		reg2 = OUT1_ISKEW_REG2;
		break;
	case CHANNEL_2:
		reg1 = OUT2_ISKEW_REG1;
		reg2 = OUT2_ISKEW_REG2;
		break;
	case CHANNEL_3:
		reg1 = OUT3_ISKEW_REG1;
		reg2 = OUT3_ISKEW_REG2;
		break;
	case CHANNEL_4:
		reg1 = OUT4_ISKEW_REG1;
		reg2 = OUT4_ISKEW_REG2;
		break;
	}

	_writeRegister(reg2, MASK_FIFT_MSB, (divider_val & MASK_FIFT_MSB), POS_FOUR);
	_writeRegister(reg1, MASK_ALL, ((divider_val & MASK_ALL_12_BIT) >> POS_THREE), POS_ZERO);
}

void     SparkFun_5P49V60::setPllFeedBackFractDiv(uint32_t divider_val)
{
	if ( divider_val > 16777215 )
		return;

	_writeRegister(FDB_FRAC_DIV_REG3, MASK_ALL, (divider_val & MASK_ALL_8_BIT), POS_ZERO);
	_writeRegister(FDB_FRAC_DIV_REG2, MASK_ALL, ((divider_val & MASK_ALL_16_BIT) >> POS_EIGHT), POS_ZERO);
	_writeRegister(FDB_FRAC_DIV_REG1, MASK_ALL, ((divider_val & MASK_ALL_24_BIT) >> POS_SIXT), POS_ZERO);
}

// Largest value that can be set: 4095. To determine this value you'll divide your desired output by the value of the
// clock source, 16MHz by default, e.g.  VCO/16MHz Clock = 100 DEC => 0x64 => 0b00001100100
void     SparkFun_5P49V60::setPllFeedbackIntDiv(uint16_t divider_val)
{
	if ( divider_val > 4095 )
		return;

	_writeRegister(FDB_INT_DIV_REG2, MASK_FIFT_MSB, (divider_val & MASK_FIFT_MSB), POS_FOUR);
	_writeRegister(FDB_INT_DIV_REG1, MASK_ALL, ((divider_val & MASK_ALL_12_BIT) >> POS_THREE), POS_ZERO);

	// Enables the changes by calibrating the VCO.
	calibrateVco();
}

void     SparkFun_5P49V60::setPllFilterCapOne(uint8_t cap_val)
{
	// Translate specified capacitance value into corresponding register value
	uint8_t bits;
	switch (cap_val) {
	case 12:
		bits = 0b000; break;
	case 16:
		bits = 0b001; break;
	case 20:
		bits = 0b010; break;
	case 24:
		bits = 0b011; break;
	case 28:
		bits = 0b100; break;
	default:
		// Undefined input
		return;
	}

	_writeRegister(RC_CONTR_REG2, MASK_SEVEN, bits, POS_ZERO);
}

void     SparkFun_5P49V60::setPllFilterCapTwo(float cap_val)
{
	// Translate specified capacitance value into corresponding register value
	uint8_t bits;
	if ( cap_val == 1.8 )
		bits = 0b001;
	else if ( cap_val == 3.6 )
		bits = 0b011;
	else if ( cap_val == 5.4 )
		bits = 0b111;
	else
		return;    // Undefined input

	_writeRegister(RC_CONTR_REG3, MASK_THR_MSB, bits, POS_FOUR);
}

// REG 0x1D, bits[:] - SKIPPED
void     SparkFun_5P49V60::setPllFilterChargePump(uint8_t pump_val)
{
}

void     SparkFun_5P49V60::setPllFilterResOne(uint16_t res_val)
{
	// Translate specified resistance value into corresponding register value
	uint8_t bits;
	switch (res_val) {
	case 1500:
		bits = 0b11110; break;
	case 46500:
		bits = 0b0; break;
	default:
		// Undefined input
		return;
	}

	_writeRegister(RC_CONTR_REG2, 0x07, bits, POS_THREE);
}

void     SparkFun_5P49V60::setPllFilterResTwo(uint16_t res_val)
{
	// Translate specified resistance value into corresponding register value
	uint8_t bits;
	switch (res_val) {
	case 1000:
		bits = 0b100; break;
	case 1450:
		bits = 0b111; break;
	case 1600:
		bits = 0b011; break;
	case 2000:
		bits = 0b001; break;
	case 5300:
		bits = 0b110; break;
	case 7000:
		bits = 0b101; break;
	case 8000:
		bits = 0b010; break;
	}

	_writeRegister(RC_CONTR_REG3, MASK_FOURT, bits, POS_ONE);
}

// Sigma Delta Modulator Setting: bypass, order one through three
void     SparkFun_5P49V60::setSigmaDeltaMod(uint8_t order)
{
	if ( order <= 3 )
		_writeRegister(FDB_FRAC_DIV_REG1, MASK_TEN, order, POS_TWO);
}

void     SparkFun_5P49V60::setVcoFrequency(float freq)
{
	//Convert to MHz
	_vco_freq = static_cast<uint16_t>(freq);
	float pll_divider = _vco_freq/_clock_freq;
	// Seperate the divider into the whole number and decimal.
	uint16_t int_portion  = static_cast<uint8_t>(pll_divider);
	float decimal  = fmod(pll_divider, int_portion);
	uint32_t fract_portion = static_cast<uint32_t>(fract_portion * pow(2,24));

	setPllFeedbackIntDiv(int_portion);
	setPllFeedBackFractDiv(fract_portion);

	//Enable VCO
	calibrateVco();
}

void     SparkFun_5P49V60::skewClock(CLOCK_CHANNEL channel, float degrees)
{
	uint8_t skew_int  = _calculate_skew_int(channel, degrees);
	float   skew_frac = _calculate_skew_frac(channel, degrees);

	setIntDivSkew(channel, skew_int);
	setFractDivSkew(channel, skew_frac);

	globalReset();
}

// Enable the VCO test mode
void     SparkFun_5P49V60::vcoTestControl(bool control)
{
	if ( control )
		_writeRegister(VCO_BAND_REG, MASK_TWO_MSB, ENABLE, POS_FIVE);
	else
		_writeRegister(VCO_BAND_REG, MASK_TWO_MSB, DISABLE, POS_FIVE);
}

void     SparkFun_5P49V60::xtalControl(bool control)
{
	if ( control ) {
		_writeRegister(SHUTDOWN_REG, MASK_EIGHT_MSB, ENABLE, POS_SEVEN);
		clockInControl(DISABLE);
	} else {
		_writeRegister(SHUTDOWN_REG, MASK_EIGHT_MSB, DISABLE, POS_SEVEN);
	}
}

/// Calculate the whole skew value required to establish a particular channel phase shift.
/// This process is defined by the VersaClock 6E Programming Guide, pg. 48
/// @param(channel)  The channel intended to be phase shifted
/// @param(degrees)  The degrees of phase shift (delay) to be induced
/// @return  The whole (integer.fraction) skew value
float    SparkFun_5P49V60::_calculate_skew_value(CLOCK_CHANNEL channel, float degrees)
{
	float feedback_divider;
	float N;
	float skew_value;

	feedback_divider = readPllFeedBackIntDiv() + readPllFeedBackFractDiv()/pow(2.0,24.0);
	// N = feedback_divider/2.0;  // This method used in the guide results in half the desired phase shift
	N = feedback_divider;

	skew_value  = N*(1.0+(degrees/360.0));
	skew_value -= trunc(N);

	return skew_value;
}

/// Calculate the fractional portion of the skew value required to establish a particular channel phase shift.
/// @param(channel)  The channel intended to be phase shifted
/// @param(degrees)  The degrees of phase shift (delay) to be induced
/// @return  The fractional portion of the skew value ( 0 <= x < 1 )
float    SparkFun_5P49V60::_calculate_skew_frac(CLOCK_CHANNEL channel, float degrees)
{
	float total_skew;
	float frac_skew;

	// Calculate the total skew value, extract only the fractional component, e.g. 0.XXX
	total_skew = _calculate_skew_value(channel, degrees);
	frac_skew  = fmod(total_skew, 1.0);
	return frac_skew;
}

/// Calculate the integer portion of the skew value required to establish a particular channel phase shift.
/// @param(channel)  The channel intended to be phase shifted
/// @param(degrees)  The degrees of phase shift (delay) to be induced
/// @return  The integer portion of the skew value
uint8_t SparkFun_5P49V60::_calculate_skew_int(CLOCK_CHANNEL channel, float degrees)
{
	float total_skew;
	uint8_t int_skew;

	// Calculate the total skew value, extract only the integer component
	total_skew = _calculate_skew_value(channel, degrees);
	int_skew   =  static_cast<uint16_t>(total_skew);
	return int_skew;
}

// Reads an eight bit register given the register's address
uint8_t  SparkFun_5P49V60::_readRegister(uint8_t reg)
{
	uint8_t reg_value;

	_i2cPort->beginTransmission(_address);
	_i2cPort->write(reg); // Moves pointer to register.
	_i2cPort->endTransmission(false);
	_i2cPort->requestFrom(_address, static_cast<uint8_t>(1));

	reg_value = _i2cPort->read();
	return reg_value;
}

// Handles I2C write commands for modifying individual bits in an 8b register. Paramaters include the
// register's address, a mask for bits that are ignored, the bits to write, and the bit's start position.
void     SparkFun_5P49V60::_writeRegister(uint8_t wReg, uint8_t mask, uint8_t bits, uint8_t startPosition)
{
	uint8_t i2cWrite;

	i2cWrite = _readRegister(wReg);
	i2cWrite &= mask;
	i2cWrite |= (bits << startPosition);

	_i2cPort->beginTransmission(_address);
	_i2cPort->write(wReg);
	_i2cPort->write(i2cWrite);
	_i2cPort->endTransmission();
}
